from threading import Thread
import logging
import time
from datetime import datetime
import numpy as np

# class FuncThread(Thread):
#     def __init__(self, name, func, args=(), is_daemon=True):
#         super().__init__(target=func, name=name, args=args)
#         self.daemon = is_daemon

LOG_LV_DICT = {'debug': logging.DEBUG, 'info': logging.INFO, 'warning': logging.WARNING, 'error': logging.ERROR,
               'critical': logging.CRITICAL}
# LOG_PATH = '/Users/lokhiufung/Documents/GitLab/logs/universe/'
LOG_PATH = 'C:/gitlab/logs/universe/'


def encode_ts(ts, divider=3600):
    # freq = freq.lower()
    value = ts % divider  # e.g 100.6666 % 10 = 0.66600000000025
    return np.cos(2 * np.pi * value / divider), np.sin(2 * np.pi * value / divider)


def str_to_ts(ts_str, format='%Y-%m-%d %H:%M:%S.%f'):
    # ts_sec = time.mktime(datetime.strptime(ts_str, format).timetuple())
    ts_sec = datetime.strptime(ts_str, format).timestamp()
    return ts_sec


def get_dates(start_date, end_date):
    from datetime import date, timedelta
    y_start, m_start, d_start = start_date.split('-')
    y_end, m_end, d_end = end_date.split('-')

    d1 = date(int(y_start), int(m_start), int(d_start))  # start date
    d2 = date(int(y_end), int(m_end), int(d_end))  # end date
    delta = d2 - d1  # timedelta

    date_list = [str(d1 + timedelta(i)) for i in range(delta.days + 1)]
    return date_list


def get_logger(logger_name, fh_log_lv, ch_log_lv, path=LOG_PATH):

    fh_log_lv = LOG_LV_DICT[fh_log_lv.lower()]
    ch_log_lv = LOG_LV_DICT[ch_log_lv.lower()]
    logger = logging.getLogger(logger_name)
    ch = logging.StreamHandler()
    log_path = time.strftime(path + logger_name + '.%Y%m%d.log')
    fh = logging.FileHandler(log_path)
    formatter = logging.Formatter('%(name)-12s %(levelname)-8s %(message)s')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    logger.addHandler(ch)
    logger.addHandler(fh)
    logger.setLevel(fh_log_lv)
    logger.setLevel(ch_log_lv)

    logger.propagate = False
    return logger
    


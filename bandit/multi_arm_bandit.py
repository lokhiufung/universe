import numpy as np
import random


class MultiArmBandit(object):
    def __init__(self, config):
        self.nb_bandits = config['nb_bandits']
        self.p_list = config['p_list']
        self.r_list = config['r_list']       
        self.steps_before_shuffle = config['steps_before_shuffle'] if 'steps_before_shuffle' in config.keys() else None
        assert len(self.p_list) == len(self.r_list) == self.nb_bandits
        self.bandits = [_Bandit(p, r) for p, r in zip(self.p_list, self.r_list)]
        self.count = 0
        self.action_space = list(range(self.nb_bandits))

    def step(self, action):
        """
        action: int, range from 0 to self.nb_bandit - 1
        """
        if self.steps_before_shuffle is not None:
            if self.count % self.steps_before_shuffle and self.count > 0 :
                random.shuffle(self.bandits)
        assert action < self.nb_bandits
        observations = {}
        reward = self.bandits[action].pull()
        done = False
        info = ''
        self.count += 1
        return observations, reward, done, info

    def render(self):
        pass

    def reset(self):
        pass

    def close(self):
        pass


class _Bandit(object):
    def __init__(self, p, r):
        """
        p: sucess rate
        r: reward
        """
        assert p < 1.0
        self.p = p
        self.r = r

    def pull(self):
        if np.random.random() < self.p:
            reward = self.r
        else:
            reward = 0.0
        return reward


if __name__ == '__main__':
    nb_trials = 100
    for p in [0.1, 0.2, 0.5, 0.8]:
        ban = _Bandit(p=p, r=1.0)
        reward_list = []
        for i in range(nb_trials):
            reward = ban.pull()
            reward_list.append(reward)
        print('actual cum reward on {} run: {} expected reward: {}'.format(nb_trials, sum(reward_list), nb_trials*p))


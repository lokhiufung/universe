import numpy as np
import h5py
import math
from collections import deque

from universe.trading_gym.acct import Account
from universe.trading_gym.chronologizer import Chronologizer 
from universe.trading_gym.engine import Engine
from universe.trading_gym.streamer import Streamer
from universe import utils


# DATA_PATH = 'D:/dl_data/BMEX'
log = utils.get_logger('trading.py', fh_log_lv='debug', ch_log_lv='debug')


class TradingEnv(object):
    """
    # filled_qty is approximated by exponential distribution
    config: {
        pdt: ...,
        exch: ...,
        bk_types: ['quote', 'trade'],
        max_recent_trade_size: ...,
        initial_bal: ...,
        leverage: ...,
        start_date: ...,
        end_date: ...,
        fee: {mfee: ..., tfee: ...,},
        filled_dist_params: {beta: ...},
        data_root_path: ...
    }
    """
    def __init__(self, config):
        self.pdt = config['pdt'].upper()
        self.exch = config['exch'].upper()
        self.start_date = config['start_date']
        self.end_date = config['end_date']
        self.tfee = config['fee']['tfee']
        self.mfee = config['fee']['mfee']
        self.bk_types = ['quote', 'trade']
        self.target = 'quote'
        self.mmd = config['mmd']
        # parameter of exponential distribution for filled_qty
        # self.filled_dist = FilledDist(config['filled_dist_params']['intervals'], config['filled_dist_params']['weights'])
        self.acct = Account(config['initial_bal'], config['leverage'])
        self.data_path = '{}/{}'.format(config['data_root_path'], self.exch)
        # print(self.data_path)
        # the first dim is bk level
        self.bk = np.zeros((1, 4))
        self.action_space = [-1, 0, 1]  # TEMP only consider action_side
        self.streamers = {}
        # self.ts_filter = TimeFilter(self.target, self.bk_types)
        for bk_type in self.bk_types:
            self.streamers[bk_type] = Streamer(self.data_path, self.pdt, bk_type, self.start_date, self.end_date, 1e6)  # FIXME
                # self.local_accum[bk_type] = deque()
        self.chronologizer = Chronologizer(self.streamers)
        self.max_recent_trade_size = config['max_recent_trade_size']
        self.engine = Engine(config['filled_dist_params'], self.mfee, self.tfee)
        self.recent_trades = []
        self.ts = 0.0  # current timestamp
        self.o_num = 0

        # just for rendering
        self.last_action = 0
        self.now = 0.0
        self.last_render_time = 0.0
        self.viewer = None

    def step(self, order):
        """
        **single product**
        observation: {'trade': .... , 'quote': .....}
        action: IOC, (post only), (side, order_type, qty, px)
        side := {-1:short, 0:stay, 1:long}, order_type:= {0:IOC, 1:post_only}, qty:={nonzero integers}, px:={float}
        """
        assert type(order) is tuple
        observations, done, reward, info = {}, True, 0.0, '' 
        # state of the market
        # stream in data
        updates = self.chronologizer.stream()
        bid, bid_qty, ask, ask_qty = self.bk[0]
        mid_px = (bid + ask) / 2  # proxy quote price for all spreads, can be negative
        log.debug('{} - (from book) bid {} bid_qty {} ask {} ask_qty {} mid_px {}'.format(self.ts, bid, bid_qty, ask, ask_qty, mid_px))
        if updates:
            # Assume NO latency issuses! 
            for channel, info_tup in updates:
                remarks = 'new'
                if self.acct.is_liquidate():
                    # create an liquidation order
                    # info += 'liquidation'
                    self._clear_order_dict()  # clear all open orders
                    order = self._create_liquidation_order()
                    remarks = 'liquidation'
                # pendding new order
                if order[0] != 0 and order[2] > 0:  # make sure order qty > 0
                    self._pendding_new_order(order, remarks)
                # added_pos_expos, entry_px, offset_qty = 0.0, 0.0, 0.0
                if channel == 'quote':
                    self._up_after_quote_event(mid_px, info_tup)
                elif channel == 'trade':
                    self._up_after_trade_event(mid_px, info_tup)
                else:
                    raise ValueError('Invalid channel: {}, possible channels are {}'.format(channel, self.bk_types))
                # up bk after matching event
                log.debug('{} - avg_px {} mid_px {}'.format(self.ts, self.acct.avg_px, mid_px))
                reward, done, acct_summary = self._get_status()
                observations['ts'] = self.ts
                observations['quote'] = self.bk[0, :]
                observations['trade'] = self.recent_trades
                observations['acct_summary'] = acct_summary

                self.now = utils.str_to_ts(self.ts)  # this is for rendering
                self.last_action = order[0]
                if self.last_render_time == 0.0:
                    self.last_render_time = self.now
                log.debug('now {} last_action {} last_render_time {}'.format(self.now, self.last_action, self.last_render_time))
        return observations, reward, done, info
    
    def _up_after_quote_event(self, mid_px, info_tup):
        ts, bid, bid_qty, ask, ask_qty = info_tup
        ts = ts.decode()  # bytes to str
        self.ts = ts
        filled_list = self._matching()  # No trade update here
        log.debug('{} - filled_list {}'.format(self.ts, filled_list))
        if filled_list:
            for filled_o in filled_list:
                self._up_filled_o(filled_o, mid_px)
            else:
                self._up_acct(is_pos_change=False, mid_px=mid_px, new_pos_side=0, added_pos_expos=0.0, offset_qty=0.0, entry_px=0.0, fee=0.0)
        self._up_bk(bid, bid_qty, ask, ask_qty)
        log.debug('{} - updated quote bid {} bid_qty {} aks {} ask_qty {}'.format(self.ts, bid, bid_qty, ask, ask_qty))

    def _up_after_trade_event(self, mid_px, info_tup):
        ts, t_px, t_side, t_qty = info_tup
        ts = ts.decode()  # bytes to str
        self.ts = ts
        t_side = 1 if t_side.decode() == 'B' else -1
        log.debug('{} - updated trade t_px {} t_side {} t_qty {}'.format(self.ts, t_px, t_side, t_qty))
        self._up_recent_trades(t_px, t_side, t_qty)
        filled_list = self._matching(t_px, t_side, t_qty)  # trade udpate happened, match post orders
        log.debug('{} - filled_list {}'.format(self.ts, filled_list))
        if filled_list:
            for filled_o in filled_list:
                self._up_filled_o(filled_o, mid_px)
            else:
                self._up_acct(is_pos_change=False, mid_px=mid_px, new_pos_side=0, added_pos_expos=0.0, offset_qty=0.0, entry_px=0.0, fee=0.0)

    def _up_bk(self, bid, bid_qty, ask, ask_qty, lv=0):
        self.bk[lv, :] = [bid, bid_qty, ask, ask_qty]
        log.debug('{} - updated bk {}'.format(self.ts, list(self.bk[lv, :])))

    def _up_recent_trades(self, t_px, t_side, t_qty):
        self.recent_trades.append((self.ts, t_px, t_side, t_qty))
        if len(self.recent_trades) > self.max_recent_trade_size:
            self.recent_trades.pop(0)
        log.debug('{} - new trades added length recent_trades {}'.format(self.ts, len(self.recent_trades)))

    def _matching(self, t_px=0.0, t_side=0, t_qty=0.0):
        return self.engine.matching(self.ts, self.bk, t_px, t_side, t_qty)

    def _clear_order_dict(self):
        self.engine.clear_order_dict()

    def _create_liquidation_order(self):
        pos_side, pos_expos = self.acct.get_position()
        bid, _, ask, _ = self.bk[0]  # get the first level of the book
        px = bid if pos_side > 0 else ask
        order = (-1 * pos_side, 0, pos_expos, px)
        return order

    def _pendding_new_order(self, order, remarks):
        odid = '{}_{}'.format(self.o_num, remarks)
        self.engine.pendding_new_order(odid, order)
        self.o_num += 1

    def _up_filled_o(self, filled_o, mid_px):
        # ts = ts.decode()  # TEMP
        # state of your account
        pos_side, pos_expos = self.acct.get_position()
        odid, side, order_type, entry_px, filled_qty, fee = filled_o  # parse a filled o
        # is_pos_change = False
        # only update the follow if there is an action
        # added_pos_expos, offset_qty = 0.0, 0.0  # reminder: init values for up_pos_margin
        is_offset = side * pos_side < 0
        offset_qty = min(filled_qty, pos_expos) if is_offset else 0.0
        added_pos_expos = filled_qty - offset_qty
        
        if is_offset:
            if added_pos_expos > 0:
                new_pos_side = side
            else:
                # completely offset, partially offset
                new_pos_side = 0 if offset_qty == pos_expos else pos_side
        else:
            new_pos_side = side
        log.debug('{} - odid {} order_type {} side {} pos_side {} filled_qty {} added_pos_expos {} entry_px {} fee {}'.format(self.ts, odid, order_type, side, pos_side, filled_qty, added_pos_expos, entry_px, fee))
        self._up_acct(True, mid_px, new_pos_side ,added_pos_expos, offset_qty, entry_px, fee)
        
    def _up_acct(self, is_pos_change, mid_px, new_pos_side ,added_pos_expos, offset_qty, entry_px, fee):
        if is_pos_change:  # update all items related to pos change
            self.acct.up_pos_side(new_pos_side)
            self.acct.up_pos_expos(added_pos_expos, offset_qty)
            self.acct.up_avg_px(entry_px, added_pos_expos, offset_qty)
            self.acct.up_realizedPnL(entry_px, fee, offset_qty)
            # log.debug('{} - (After filled) avg_px {} prev_avg_px {} pos_side {} pos_expos {} realizedPnL {}'.format(ts, self.acct.avg_px, self.acct.prev_avg_px, self.acct.pos_side, self.acct.pos_expos, self.acct.realizedPnL))
        self.acct.up_unrealizedPnL(mid_px)  # up unrealizedPnL bc it is needed for pos_margin
        self.acct.up_pnl()
        self.acct.up_pos_margin(added_pos_expos, entry_px, offset_qty)
        self.acct.up_wallet_bal()
        self.acct.up_margin_bal()
        self.acct.up_available_bal()
        log.debug('{} - unrealizedPnL {} pos_margin {} '
                'wallet_bal {} margin_bal {} available_bal {}'.format(self.ts, self.acct.unrealizedPnL, self.acct.pos_margin, self.acct.wallet_bal, self.acct.margin_bal, self.acct.available_bal))

    def _get_status(self):
        reward = self.acct.get_pnl_delta()
        # obs of any acct update
        acct_summary = self.acct.get_acct_summary()
        # if self.acct.is_bankrupt():
        #     # info += 'bankrupt'
        if acct_summary['wallet_bal'] < acct_summary['deposit'] * (1 - self.mmd):
            done = True
        else:
            done = False
        log.debug('{} - reward {} done {} acct_summary {}'.format(self.ts, reward, done, acct_summary))
        return reward, done, acct_summary
        
    def reset(self):
        self.acct.reset()
        # reminder: dont need to reset streamers, just acct
        # self.streamers = {}
        # self.ts_filter = TimeFilter(self.target, self.bk_types)
        # for bk_type in self.bk_types:
        #     self.streamers[bk_type] = Streamer(self.data_path, bk_type, self.pdt, self.start_date, self.end_date, 1e6)  # FIXME

    def render(self):
        # pyglet later ...
        # just taking snapshots for debugging for the moment
        if self.viewer is None:
            from universe.trading_gym.rendering import Camera
            self.viewer = Camera()
        else:
            self.viewer.update_action_list(self.last_action)
            self.viewer.update_px_list((self.bk[0, 0] + self.bk[0, 2]) / 2) 
            self.viewer.update_ts_list(self.ts)
            log.debug('(Before rendering) len action_list {} px_list {} ts_list {}'.format(len(self.viewer.action_list), len(self.viewer.px_list), len(self.viewer.ts_list)))
            if self.now - self.last_render_time > self.viewer.time_interval:
                self.viewer.draw()
                self.viewer.reset()
                self.last_render_time = self.now
                log.debug('(After rendering) len action_list {} px_list {} ts_list {}'.format(len(self.viewer.action_list), len(self.viewer.px_list), len(self.viewer.ts_list)))
                

    def close(self):
        pass

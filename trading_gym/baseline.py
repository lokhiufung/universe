class TrendFollower(object):
    def __init__(self):
        self.fast_ma = 0.0
        self.slow_ma = 0.0
        self.fast_tick = 500
        self.slow_tick = 1000
        self.long_threshold = 4.0
        self.short_threshold = 4.0
        self.max_pos_expos = 10000
        self.count = 0

    def up_ma(self, px):
        self.fast_ma += (1 / self.fast_tick) * (px - self.fast_ma)
        self.slow_ma += (1 / self.slow_tick) * (px - self.slow_ma)
        self.count += 1

    def act(self, pos_side, pos_expos, order_qty):
        side = 0
        if self.count >= max(self.fast_tick, self.slow_tick):
            if self.fast_ma - self.slow_ma > self.long_threshold:
                side = 1
            elif self.slow_ma - self.fast_ma > self.short_threshold:
                side = -1
            # offset as quick as possible
            if side != 0 and side * pos_expos < 0:
                    order_qty = max(order_qty, pos_expos)
            if pos_expos + order_qty > self.max_pos_expos and side * pos_side > 0:
                side = 0
        return side, 0, order_qty

         
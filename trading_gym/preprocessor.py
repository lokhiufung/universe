import numpy as np

class Prepocessor(object):
    def __init__(self, max_lv, t_step_size, mask=None):
        """
        max_lv: int, number of levels in quote bk 
        step_size: dict, number of obs to concat for quote/trade

        quote: ts, bid, bid_qty, ask, ask_qty
        (trade): t_ts, t_px, t_side, t_qty, (may have multiple recent trade 5~20)
        acct_summary: pos_side, pos_expo, rPnL, uPnL, deposit, pos_margin, order_margin, margin_bal, wallet_bal, available_bal
        trade_event: filled_qty, entry_px

        # preprocessing on observations, try to be minimal...
        quote -> log transformation
        trade -> log transformation (t_px), sided_log(t_qty)
        acct_summary -> sided_log(pos_expos), else log transformation
        trade_event -> log transformation
        """
        self.t_step_size = t_step_size
        self.max_lv = max_lv 
        self.bk_dim = (self.max_lv, 4)
        self.bk = np.zeros(self.bk_dim)
        # self.prev_bk = np.zeros_like(self.bk)
        # self.mask = mask
        # info vector
        self.t_history = np.zeros((self.t_step_size, 2))
        self.input_dim = len(self.bk.ravel()) + len(self.t_history.ravel()) + 7

    # def up_bk_delta_history(self, bid, bid_qty, ask, ask_qty, lv=0):
    #     bk_delta = np.zeros_like(self.bk)
    #     if len(self.bk_delta_history) > 2:
    #         self.bk[lv, :] = [bid, bid_qty, ask, ask_qty]
    #         self.prev_bk = self.bk.copy()  # copy an object 
    #         # up bk_delta only at the updated lv
    #         bk_delta[lv, :] = np.log(self.bk[lv, :]) - np.log(self.prev_bk[lv, :])
    #     if self.mask is not None:
    #         # mask some lvs
    #         bk_delta = bk_delta * self.mask
    #     self.bk_delta_history.append(bk_delta)

    def preprocess_bk(self, bid, bid_qty, ask, ask_qty, lv=0):
        # preprocessing
        self.bk[lv, :] = np.log([bid, bid_qty, ask, ask_qty])
        
    def preprocess_trade_history(self, t_px, t_side, t_qty):
        self.t_history = np.roll(self.t_history, shift=-1, axis=0)
        # preprocessing
        self.t_history[-1] = np.log([t_px, t_qty])  # log tradsformation on px and abs qty
        self.t_history[-1, 1] = t_side * self.t_history[-1, 1]  # added side to t_qty

    def preprocess_acct_summary(self, acct_summary):
        acct_summary_vec = [value for value in list(acct_summary.values())[1:]]  # the first element is pos_side
        acct_summary_vec[0] = acct_summary['pos_side'] * acct_summary_vec[0]
        return np.array(acct_summary_vec)

    def preprocess_trade_event(self, trade_event):
        # preprocessing 
        return np.log(np.array(trade_event.values()))

    # def up_acct_summary(self, acct_summary):
    #     pass

    def get_obs_vec(self, bid, bid_qty, ask, ask_qty, acct_summary, t_px, t_side, t_qty):
        # quote book
        # len_bk_delta_history = len(self.bk_delta_history)
        # if len_bk_delta_history < self.q_step_size:
        #     padding = [np.zeros_like(self.bk)] * (self.q_step_size - len_bk_delta_history)
        #     bk_delta_history = np.array(padding + self.bk_delta_history)
        # else:
        #     bk_delta_history = np.array(self.bk_delta_history)
        # bk_delta_history = np.expand_dims(bk_delta_history.ravel(), axis=1)
        ############
        # combine all values inot a single obs vector
        self.preprocess_bk(bid, bid_qty, ask, ask_qty)
        if t_px > 0.0:
            self.preprocess_trade_history(t_px, t_side, t_qty)
        bk_vec = self.bk.ravel()
        t_vec = self.t_history.ravel()
        acct_summary_vec = self.preprocess_acct_summary(acct_summary)
        # trade_event_vec = self.preprocess_trade_event(trade_event)
        obs_vec = np.concatenate([bk_vec, t_vec, acct_summary_vec])
        return obs_vec.reshape((-1, self.input_dim))
        

        





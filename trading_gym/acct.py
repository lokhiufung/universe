import numpy as np

from universe import utils
log = utils.get_logger('acct.py', fh_log_lv='error', ch_log_lv='error')


class Account(object):
    """
    Order of updating filled position
    up_pos_side/expos -> up_avg_px -> up_realizedPnL -> *up_unrealizedPnL -> up_pos_margin -> up_pnl -> up_wallet_bal -> up_margin_bal -> up_available_bal
    """
    def __init__(self, deposit, leverage):
        self.leverage = leverage
        self.realizedPnL = 0.0
        self.unrealizedPnL = 0.0
        self.order_margin = 0.0
        self.pos_margin = 0.0
        self.pos_margin_without_unrealizedPnL = 0.0
        self.pos_expos = 0.0
        self.pos_side = 0
        self.prev_pos_expos = 0.0
        self.prev_pos_side = 0
        self.avg_px = 0.0
        self.prev_avg_px = 0.0
        self.deposit = deposit
        self.prev_pnl = 0.0 
        self.pnl = self.unrealizedPnL + self.realizedPnL
        self.wallet_bal = self.deposit + self.realizedPnL
        self.margin_bal = self.wallet_bal + self.unrealizedPnL
        self.available_bal = self.margin_bal - self.order_margin - self.pos_margin
        self.last_act_ts = None

    def up_last_act_ts(self, ts):
        self.last_act_ts = ts

    def up_pnl(self):
        # Only realizedPnL is concerned !
        self.prev_pnl = self.pnl
        self.pnl = self.realizedPnL
        
    def up_realizedPnL(self, entry_px, fee, offset_qty=0.0):
        self.realizedPnL += self.prev_pos_side * offset_qty * (1 / (self.prev_avg_px + 1e-5) - 1 / entry_px) - fee
        log.debug('realizedPnL {} fee {} offset_qty {} avg_px {}'.format(self.realizedPnL, fee, offset_qty, self.avg_px))
        
    def up_unrealizedPnL(self, mid_px):
        self.unrealizedPnL = self.pos_side * self.pos_expos * (1 / (self.avg_px + 1e-5) - 1 / mid_px)
        log.debug('realizedPnL {} mid_px {} avg_px {} pos_expos {}'.format(self.realizedPnL, mid_px, self.avg_px, self.pos_expos))

    def up_wallet_bal(self):
        self.wallet_bal = self.deposit + self.realizedPnL
        log.debug('wallet_bal {} deposit {} realizedPnL {}'.format(self.wallet_bal, self.deposit, self.realizedPnL))

    def up_margin_bal(self):
        self.margin_bal = self.wallet_bal + self.unrealizedPnL
        # log.debug('margin_bal {} unrealizedPnL {}'.format(self.margin_bal, self.unrealizedPnL))
        
    def up_available_bal(self):
        self.available_bal = self.margin_bal - self.order_margin - self.pos_margin
        log.debug('available_bal {} margin_bal {} order_margin {} pos_margin {}'.format(self.available_bal, self.margin_bal, self.order_margin, self.pos_margin))       

    def up_pos_margin(self, added_pos_expos, entry_px, offset_qty=0.0):
        # offset_qty == 0 -> no pos/ clean pos -> self.avg_px != 0
        # self.avg_px == 0 -> no pos -> must be non offset
        released_margin = self.pos_margin_without_unrealizedPnL if offset_qty == self.prev_pos_expos else (offset_qty / (self.prev_avg_px + 1e-5)) / self.leverage
        self.pos_margin_without_unrealizedPnL += - released_margin + (added_pos_expos / (entry_px + 1e-5)) / self.leverage
        # self.pos_margin_without_unrealizedPnL += (- self.pos_margin_without_unrealizedPnL * offset_qty / (self.pos_expos + 1e-5) + added_pos_expos / (entry_px + 1e-5)) / self.leverage
        self.pos_margin = self.pos_margin_without_unrealizedPnL + max(0.0, self.unrealizedPnL)
        log.debug('pos_margin {} unrealizedPnL {} added_pos_expos {} offset_qty {} entry_px {}'.format(self.pos_margin, self.unrealizedPnL, added_pos_expos, offset_qty, entry_px))

    def up_pos_side(self, pos_side):
        self.prev_pos_side = self.pos_side
        self.pos_side = pos_side

    def up_pos_expos(self, added_pos_expos, offset_qty=0.0):
        self.prev_pos_expos = self.pos_expos
        self.pos_expos += added_pos_expos - offset_qty
        log.debug('pos_expos {} added_pos_expos {} offset_qty {}'.format(self.pos_expos, added_pos_expos, offset_qty))
        
    def get_position(self):
        # TEMP
        return self.pos_side, self.pos_expos

    def get_acct_summary(self):
        acct_summary = {'pos_side': self.pos_side, 'pos_expos': self.pos_expos, 'rPnL': self.realizedPnL, 'uPnL': self.unrealizedPnL, 'deposit': self.deposit, 'pos_margin': self.pos_margin, 'wallet_bal': self.wallet_bal, 'available_bal': self.available_bal}
        # acct_summary = np.array(self.pos_side, self.pos_expos, self.realizedPnL, self.unrealizedPnL, self.deposit, self.pos_margin, self.wallet_bal, self.available_bal)
        return acct_summary

    def get_pnl_delta(self):
        delta = self.pnl - self.prev_pnl
        return delta

    def close_position(self):
        pass

    def is_bankrupt(self):
        # no wallet bal or no pos_expos and no available_bal
        return self.wallet_bal < 0.0 or (self.pos_expos < 1e-5 and self.available_bal < 0.0)

    def is_liquidate(self):
        return self.available_bal < 0.0

    def reset(self):
        self.realizedPnL = 0.0
        self.unrealizedPnL = 0.0
        self.order_margin = 0.0
        self.pos_margin = 0.0
        self.pos_expos = 0.0
        self.pos_side = 0
        self.avg_px = 0.0
        self.prev_avg_px = 0.0
        self.prev_pnl = 0.0 
        self.pnl = 0.0
        self.last_act_ts = None
        self.wallet_bal = self.deposit - self.realizedPnL
        self.margin_bal = self.wallet_bal + self.unrealizedPnL
        self.available_bal = self.margin_bal - self.order_margin - self.pos_margin

    def up_avg_px(self, entry_px, added_pos_expos, offset_qty):
        # in btc 
        self.prev_avg_px = self.avg_px
        if offset_qty == 0.0:
            self.avg_px = (self.prev_pos_expos * self.avg_px + added_pos_expos * entry_px) / (self.prev_pos_expos + added_pos_expos)
        else:
            if self.pos_expos > 0.0:
                if added_pos_expos > 0.0:
                    self.avg_px = entry_px
            else:
                self.avg_px = 0.0
        log.debug('avg_px {} prev_avg_px {} entry_px {} added_pos_expos {} pos_expos {} is_offset {}'.format(self.avg_px, self.prev_avg_px, entry_px, added_pos_expos, self.pos_expos, offset_qty > 0))
        

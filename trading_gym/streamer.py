import h5py 
import math
import os
from universe import utils


class Streamer(object):
    def __init__(self, data_path, pdt, bk_type, start_date, end_date, k):
        self.data_path = data_path
        self.start_date = start_date
        self.end_date = end_date
        self.dates = utils.get_dates(self.start_date, self.end_date)
        self.num_dates = len(self.dates)
        self.num_obs_in_chunk = 0  # number of observations in chunk
        self.num_chunks = 0  # number of chunks in a day
        self.bk_type = bk_type  # quote or trade
        self.pdt = pdt
        self.chunk = None  # chunk read in
        self.bk = None  # selected hf dataset
        self.hf = None  # hf File reader

        self.from_ = 0  # chunk start
        self.to = 0  # approximate chunk end
        self.k = k  # desired chunk size
        self.idx = 0  # current idx of obs in the chunk
        self.idx_chunk = 0  # current idx of chunk
        self.idx_date = 0  # current idx of date

        self.max_shape = 0  # size of quote bk in a day
        self.chunk_size = 0  # corrected chunk size with max_shape and k
        self.is_end = False  # is the end of streaming
        # init chunks
        self.prepare_date_loop()
        self.prepare_chunk_loop()
        
    def stream(self):
        if not self.is_end:
            bk_update = self.chunk[self.idx]
            self.idx += 1
            if self.is_end_of_chunk():
                if self.is_last_chunk():
                    if self.is_last_date():
                        self.is_end = True
                        # end .....
                    else:
                        self.idx_date += 1
                        self.prepare_date_loop()
                        self.prepare_chunk_loop()
                else:
                    self.idx_chunk += 1
                    self.prepare_chunk_loop()
        else:
            bk_update = ()
        return bk_update

    def is_end_of_chunk(self):
        is_end = self.idx == self.num_obs_in_chunk
        return is_end

    def is_last_chunk(self):
        is_end = self.idx_chunk == self.num_chunks - 1
        return is_end
             
    def is_last_date(self):
        is_end = self.idx_date == self.num_dates - 1
        return is_end

    def prepare_chunk_loop(self):
        self.idx = 0  # reset idx to 0
        self.to = self.from_ + self.chunk_size
        self.chunk = self.bk[self.from_:self.to]
        self.from_ = self.to  # reset from_ after reading in chunk
        self.num_obs_in_chunk = self.chunk.shape[0]

    def prepare_date_loop(self):
        self.hf = h5py.File('{}/{}.h5'.format(self.data_path, self.dates[self.idx_date]), 'r')
        self.bk = self.hf[self.pdt][self.bk_type]
        self.max_shape = self.bk.shape[0]
        self.chunk_size = min(self.max_shape, self.k)
        self.num_chunks = int(math.ceil(self.max_shape / self.chunk_size))
        self.idx_chunk = 0
        self.from_ = 0


def stream(data_path, pdt, bk_type, start_date, end_date, k):
    """
    Streamer: quote book, trade book
    Each has its own timestamp
    """
    dates = utils.get_dates(start_date, end_date)
    for date in dates:
        hf = h5py.File('{}/{}.h5'.format(data_path, date), 'r')

        bk = hf[pdt][bk_type]
        max_shape = bk.shape[0]
        chunk_size = min(max_shape, k)
        num_chunks = int(math.ceil(max_shape / chunk_size))
        from_ = 0
        for i in range(num_chunks):
            to = from_ + chunk_size
            chunk = bk[from_:to]
            for j in range(chunk.shape[0]):
                # ts, bid, bid_qty, ask, ask_qty
                bk_update = chunk[j]
                yield bk_update
            from_ = to  # to becomes the first index


if __name__ == '__main__':

    data_path = 'D:/dl_data/BMEX'
    pdt = 'XBTUSD'
    bk_type = 'quote'
    start_date = '2018-01-01'
    end_date = '2018-01-01'
    k = 100000
    streamer = Streamer(data_path, pdt, bk_type, start_date, end_date, k)
    while True:
        bk_update = next(streamer)
        print(bk_update)
        if bk_update == ():
            break
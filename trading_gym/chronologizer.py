import numpy as np
from universe import utils


log = utils.get_logger('chronologizer.py', fh_log_lv='debug', ch_log_lv='debug')


class Chronologizer(object):
    def __init__(self, streamers):
        self.streamers = streamers 
        self.channels = list(streamers.keys())[:]  # copy channels from keys
        self.channel_to_idx = {idx: channel for idx, channel in enumerate(self.channels)}
        # self.current_channel = ''
        # self.is_vacant = {channel: True for channel in self.channels}
        self.temp_dict = {channel: () for channel in self.channels}
        self.ts_num_vec = np.zeros(len(self.channels))
        self.smallest_ts_num = np.inf
        self.last_ts_num = 0.0
    
    def _read(self, channel):
        # log.debug('Start reading {}!'.format(channel))
        info_tup = self.streamers[channel].stream()
        ts = info_tup[0]
        if type(ts) is bytes or type(ts) is np.bytes_:
            ts = ts.decode()
        else:
            raise TypeError('Expected str, got {}'.format(type(ts)))
        ts_num = utils.str_to_ts(ts)
        self.ts_num_vec[self.channels.index(channel)] = ts_num
        # self.ts_num_vec[self.channels.index(channel)] = ts_num
        self.temp_dict[channel] = info_tup

    def stream(self):
        # 1. read data if there is no data in channel
        log.debug('current temp_dict {}'.format(self.temp_dict))
        for channel in self.channels:
            if not self.temp_dict[channel]:
                log.debug('updated channel {} temp_dict[{}] {}'.format(channel, channel, self.temp_dict[channel]))
                self._read(channel)
            assert self.temp_dict[channel]  # TEMP
        # 2. find the smallest ts_num
        smallest_ts_num = np.amin(self.ts_num_vec)
        log.debug('streaming smallest_ts_num {} temp_dict {}'.format(smallest_ts_num, self.temp_dict))
        output_list = []
        # 3. pop data from channel with the smallest ts_num
        for channel in self.channels:
            # print(self.temp_dict[channel])
            ts_num = self.ts_num_vec[self.channels.index(channel)]
            info_tup = self.temp_dict[channel]
            if abs(ts_num - smallest_ts_num) < 1e-9:
                log.debug('output channel {} ts_num {} smallest_ts_num {} delta {} info_tup {}'.format(channel, ts_num, smallest_ts_num, ts_num - smallest_ts_num, info_tup))
                output_list.append((channel, info_tup))
                assert self.last_ts_num <= smallest_ts_num
                # reset 
                self.temp_dict[channel] = ()
                # self.ts_num_vec[self.channels.index(channel)] = 0.0
        return output_list

    def renew_smallest_ts_num(self):
        pass
    

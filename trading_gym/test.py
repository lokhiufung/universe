import math
import pyglet
import numpy as np
from pyglet.gl import *

BG_COLOR = (0, 0, 0, 0)
# win = pyglet.window.Window(width=800, height=600)

# class Point(object):
#     def __init__(self):
#         pass
#         # Geom.__init__(self)
#     def render1(self):
#         glBegin(GL_POINTS) # draw point
#         glColor4f(255, 0, 0, 1.0)
#         glVertex3f(100, 100, 100)
#         glEnd()

# class PolyLine:
#     def __init__(self, v):
#         self.v = v

#     def render1(self):
#         glColor4f(0, 0, 0, 0.5)        
#         glBegin(GL_LINE_STRIP)
#         for p in self.v:
#             glVertex3f(p[0], p[1],0)  # draw each vertex
#         glEnd()

# class Board(object):
#     def __init__(self, loc, width, height):
#         self.loc = loc
#         self.width = width
#         self.height = height
#         self.v = [(100, 100), (200, 100), (100, 200), (200, 200), (100, 100)]
#         # self.vertex_list = pyglet.graphics.vertex_list(4, ('v2i', (100,100, 100,200, 200,100, 200,200)))

#     def render(self):
#         # self.vertex_list.draw(GL_POINTS)
#         glBegin(GL_POLYGON)
#         for p in self.v:
#             glVertex3f(p[0], p[1],0)  # draw each vertex
#         glEnd()
        

# class Chart(object):
#     def __init__(self, v):
#         self.v = v

#     def update_price(self, v_new):
#         self.v.append(v_new)
#         if len(self.v) > 1000:
#             self.v.pop(0)

#     def render(self):
#         pass

# class Action(object):
#     def __init__(self, v):
#         self.v = v

#     def render(self):
#         pass

# class Viewer(pyglet.window.Window):
#     def __init__(self, width, height, caption='gym'):
#         super(Viewer, self).__init__(width=width, height=height, caption=caption)
#         self.score = 0.0
        
#         self.score_label = pyglet.text.Label(text='Score: {}'.format(0.0), x=self.width // 2, y=self.height - 50)
#         self.board = Board(loc=(0, 0), width=500, height=500)
#         glClearColor(*BG_COLOR)  # clear the backgroud color
#         # glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
#         # glEnable(GL_BLEND)
#         pyglet.clock.schedule_interval(self.render, 0.005)  # one of args must be dt (seconds since last callback) the last arg is the arg of update_frame function

#     def init_clock_schedule(self, callback, kwargs):
#         pass

#     def render(self, dt):
#         self.update_score()
#         self.draw_elements()

#     def update_board(self):
#         pass

#     def update_score(self):
#         if np.random.random() > 0.7:
#             self.score += 1.0
#         self.score_label.text = 'Score: {}'.format(self.score)
    
#     def update_chart(self):
#         pass

#     def draw_elements(self):
#         glClearColor(*BG_COLOR)  # clear the backgroud color
#         self.clear()
#         self.score_label.draw()
#         # self.board.vertex_list.draw()
#         self.board.render()




# if __name__ == '__main__':
#     viewer = Viewer(width=1024, height=800)
#     pyglet.app.run()


from universe import utils


log = utils.get_logger('engine.py', fh_log_lv='debug', ch_log_lv='debug')


class Engine(object):
    def __init__(self, filled_dist_params, mfee, tfee):
        self.mfee = mfee
        self.tfee = tfee
        self.filled_dist_params = filled_dist_params
        self.order_dict = {}
        # self.liquidation_odid = 0
        # self.filled_dist = FilledDist(filled__dist_params['intervals'], filled__dist_params['weights'])

    def matching(self, ts, bk, t_px, t_side, t_qty):
        matching_list = []
        log.debug('{} - order_dict {}'.format(ts, self.order_dict))
        for odid in list(self.order_dict.keys())[:]:
            side, order_type, qty, px = self.order_dict[odid]
            bid, bid_qty, ask, ask_qty = bk[0, :]  # get the first level of the book
            entry_px, filled_qty, fee = 0.0, 0.0, 0.0
            if order_type == 0:
                # IOC
                if (side > 0 and px >= ask and ask_qty >= qty) or (side < 0 and px <= bid and bid_qty >= qty):
                    entry_px = min(px, ask) if side > 0 else max(px, bid) 
                    filled_qty = qty
                    fee = self.tfee * filled_qty / entry_px
                del self.order_dict[odid]  # remove order after matched/canceled
            elif order_type == 1:
                # POST
                # if (side > 0 and px >= t_px and t_qty >= qty and t_side == -1) or (side < 0 and px <= t_px and t_qty >= qty and t_side == 1):
                #     entry_px, filled_qty = px, min(t_qty, qty)
                #     fee = self.mfee * filled_qty / entry_px
                # FIXME: update new qty for filled post order
                raise NotImplementedError('order_type {}'.format(order_type))
            else:
                raise ValueError('Invalid order_type: {}'.format(order_type))
            log.debug('{} - side {} order_type {} bid {} bid_qty {} ask {} ask_qty {} entry_px {} filled_qty {}'.format(ts, side, order_type, bid, bid_qty, ask, ask_qty, entry_px, filled_qty))
            # If nothing filled, dont append
            if filled_qty > 0.0:
                matching_list.append((odid, side, order_type, entry_px, filled_qty, fee))
        return matching_list 

    # def pendding_liquidation_order(self, ts, bk, pos_expos, pos_side):
    #     # TEMP: should be market order
        
    #     self.pendding_new_order(self.liquidation_odid, order)
    #     # self.liquidation_odid += 1

    def pendding_new_order(self, odid, order):
        self.order_dict[odid] = order

    def clear_order_dict(self):
        self.order_dict = {}

    # def _is_filled():
    #     pass

    # def _parse_order(self, order):
    #     """
    #     order: IOC, (post only), (side, order_type, qty, px)
    #     side := {-1:short, 0:stay, 1:long}, order_type:= {0:IOC, 1:post_only}, qty:={nonzero integers}
    #     """
    #     side, order_type, qty, px = order
    #     return side, order_type, qty, px
    
# class FilledDist(object):
#     def __init__(self, intervals, weights):
#         self.intervals = intervals
#         self.weights = weights  # proba that traded qty fall into this interval
#         self.size = len(self.intervals)

#     def sample(self):
#         idx_interval = np.random.choice(range(self.size), p=self.weights)
#         return np.random.uniform(low=self.intervals[idx_interval][0], high=self.intervals[idx_interval][1])


    

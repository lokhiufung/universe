from collections import deque
from universe import utils

log = utils.get_logger(logger_name='time_filter.py', fh_log_lv='error', ch_log_lv='error')

@DeprecationWarning
class TimeFilter(object):
    def __init__(self, target, channels):
        self.last_ts = {}
        self.local_accum = {}
        self.locks = {}
        self.channels = channels
        self.target = target
        # self.streamers = {}
        for channel in channels:
            self.last_ts[channel] = 0.0
            self.local_accum[channel] = deque()
            self.locks[channel] = {}
            for type_ in ['input', 'output']:
                self.locks[channel][type_] = False
    
    def lock(self, channel, type_):
        self.locks[channel][type_] = True
        log.debug('(Lock) {} channel {} last_ts {} target {} '
                  'target_last_ts {}'.format(type_.upper(), channel, self.last_ts[channel], self.target, self.last_ts[self.target]))

    def release(self, channel, type_):
        self.locks[channel][type_] = False
        log.debug('(Release) {} channel {} last_ts {} target {} '
                  'target_last_ts {}'.format(type_.upper(), channel, self.last_ts[channel], self.target, self.last_ts[self.target]))

    def is_lock(self, channel, type_):
        return self.locks[channel][type_]

    def up_last_ts(self, channel, ts):
        self.last_ts[channel] = ts
        log.debug('channel {} last_ts {} target {} '
                  'target_last_ts {}'.format(channel, self.last_ts[channel], self.target, self.last_ts[self.target]))

    def up_locks(self):
        for channel in self.channels:
            if channel != self.target:
                if self.last_ts[channel] > self.last_ts[self.target] and not self.is_lock(channel, 'output'):
                    self.lock(channel, 'output')
                    self.lock(channel, 'input')
                elif self.last_ts[channel] <= self.last_ts[self.target] and self.is_lock(channel, 'output'):
                    self.release(channel, 'output')
                    self.release(channel, 'input')

    def up_local_accum(self, channel, obs):
        self.local_accum[channel].append(obs)
            
import cython


cdef class Streamer:
    cdef:
        str data_path, start_date, end_date, bk_type, pdt
        list dates
        int num_dates, num_obs_in_chunk, num_chunks, from_, to, k, idx, idx_chunk, idx_date, max_shape, chunk_size
        object chunk, bk, hf
        bint is_end

    @cython.locals(bk_update=cython.tuple)
    cpdef tuple stream(self)

    @cython.locals(is_end=cython.bint)
    cdef bint is_end_of_chunk(self) except *

    @cython.locals(is_end=cython.bint)
    cdef bint is_last_chunk(self)

    @cython.locals(is_end=cython.bint)
    cdef bint is_last_date(self)

    cdef void prepare_chunk_loop(self) except *
        
    cdef void prepare_date_loop(self) except *
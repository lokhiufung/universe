# import pyglet
import matplotlib.pyplot as plt
# from multiprocessing import Process, Pipe 
from datetime import datetime
import time
import os
import numpy as np
from universe import utils

# PATH = '/Users/lokhiufung/desktop'
PATH = utils.LOG_PATH
# TEMP
TEMP_CONFIG_PATH = '/Users/lokhiufung/Documents/GitLab/logs/universe'

# Turn interactive plotting off
plt.ioff()


class Camera(object):
    """
    regularly taking snapshot of the training process
    """
    def __init__(self, time_interval=3600):
        # self.log_path = log_path
        self.time_interval = time_interval
        self.ts_list = []
        self.px_list = []
        self.action_list = []
        # self.start_time = None
        # self.end_time = None
        # create a new folder for every training time
        self.cache_folder = '{}/{}'.format(PATH, str(time.time())[:-7])
        os.mkdir(self.cache_folder)

    def update_ts_list(self, new_ts):
        self.ts_list.append(new_ts)

    def update_action_list(self, new_action):
        self.action_list.append(new_action)        

    def update_px_list(self, new_px):
        self.px_list.append(new_px)

    def reset(self):
        # self.start_time = None
        # self.end_time = None
        self.ts_list, self.px_list, self.action_list = [], [], []   

    def transform_action_to_color(self, action):
        if action == 1:
            color = 'green'
        elif action == -1:
            color = 'red'
        else:
            color = 'black'
        return color

    def transform_action_to_label(self, action):
            if action == 1:
                label = 'buy'
            elif action == -1:
                label = 'sell'
            else:
                label = 'No action'
            return label

    def draw(self):
        self.ts_list = np.array(self.ts_list, dtype=np.datetime64)
        fig, ax = plt.subplots(1, 1, figsize=(30, 20)) 
        ax.plot(self.ts_list, self.px_list, label='mid_px', color='black', linestyle='--')
        colors = list(map(self.transform_action_to_color, self.action_list))
        # labels = list(map(self.transform_action_to_label, self.action_list))
        ax.scatter(self.ts_list, self.px_list, c=colors)
        ax.legend()

        folder = '{}/{}'.format(self.cache_folder, str(np.datetime64(self.ts_list[-1], 'D')))
        if not os.path.exists(folder):
            os.mkdir(folder)
        fig.savefig('{}/{}.png'.format(folder, time.time()))
        # plt.show()

        # reset all lists after rending
        # if self.end_time - self.start_time > self.time-interval:
        #     self.ts_list, self.px_list, self.action_list = [], [], []   

def plot_q_value():
    """
    By grepping logs
    """

    date = str(datetime.now().date()).replace('-', '')
    # path = '{}/{}.{}.log'.format(TEMP_CONFIG_PATH, 'gym_room.py', date)
    path = '{}/{}'.format(TEMP_CONFIG_PATH, 'q_values.txt')
    
    q_value_list= []
    start = time.perf_counter()
    counter = 0
    with open(path, 'r') as f:
        for row in f:
            split = row.split(' ')
            if 'max_q_value' in split:
                idx_q_value = split.index('max_q_value')
                q_value = float(split[idx_q_value + 1])
                q_value_list.append(q_value)
    print('Grep time: {}'.format(time.perf_counter() - start))
    q_value_series = pd.Series(q_value_list)
    fig, ax = plt.subplots()
    ax.plot(q_value_series, label='Q value', color='blue')
    ax.plot(q_value_series.rolling(10000).mean(), color='blue', alpha=0.4)
    ax.legend()
    plt.show()

if __name__ == '__main__':
    cam = Camera()
    px = 1.0
    for i in range(1000):
        cam.update_px_list(px * np.exp(1.0 + np.random.randn(1)))
        cam.update_action_list(np.random.choice([-1, 0, 1], p=[0.1, 0.8, 0.1]))
        cam.update_ts_list(str(datetime.now()))
    # print(cam.px_list)
    # print(cam.action_list)
    # print(cam.ts_list)
    cam.draw()
    cam.reset()
    print(cam.px_list)
    print(cam.action_list)
    print(cam.ts_list)

            
from collections import deque
import numpy as np
import time
import random

from keras.models import Model, load_model
from keras.layers import Dense, Input, Flatten
from keras.optimizers import RMSprop

from universe import utils

CONFIG_PATH = '/Users/lokhiufung/Documents/GitLab/universe/nn_config/'
log = utils.get_logger(logger_name='agent_dqn.py', fh_log_lv='debug', ch_log_lv='debug')


class AgentDQN(object):
    def __init__(self, input_dim, action_dim, replay_size=10000, batch_size=1024, gamma=0.9, learning_rate=0.05, tau=0.5, is_load=False, name=''):
        self.name = name
        self.input_dim = input_dim
        self.action_dim = action_dim
        # self.epsilon = epsilon
        self.lr = learning_rate
        self.tau = tau  # update rate
        self.gamma = gamma
        self.batch_size = batch_size
        self.replay_size = replay_size
        self.memory = deque(maxlen=self.replay_size)
        if is_load and self.name:
            self.action_nn, self.target_nn = self.load_model(self.name)
        else:
            self.action_nn = self.build_nn()
            self.target_nn = self.build_nn()
        self.action_nn.compile(loss='mse', optimizer=RMSprop(lr=self.lr))
        self.target_nn.compile(loss='mse', optimizer=RMSprop(lr=self.lr))


    def build_nn(self):
        input_ = Input(shape=self.input_dim)
        # x = Flatten()(input_)
        x = Dense(64, activation='relu')(input_)
        x = Dense(self.action_dim, activation='linear')(x)

        model = Model(input_, x)
        return model

    def act(self, obs):
        # the index of action with the highest q value
        q_values = self.action_nn.predict(obs)
        action = np.argmax(self.action_nn.predict(obs), axis=1)[0]
        return q_values, action

    def replay(self):
        minibatch = random.sample(self.memory, self.batch_size)
        targets, states = [], []
        for prev_obs, action, reward, obs, done in minibatch:
            if not done:
                # target = reward + self.gamma * np.amax(self.target_nn.predict(obs)[0])  # Q learning 
                target = reward + self.gamma * self.target_nn.predict(obs)[0][np.argmax(self.action_nn.predict(obs), axis=1)[0]]  # double Q learning
            else:
                target = reward
            target_q = self.action_nn.predict(prev_obs)  # only evaluate action that acturally acted
            target_q[0][action] = target
            targets.append(target_q)
            states.append(prev_obs)
        targets = np.array(targets).reshape((-1, self.action_dim))
        states = np.squeeze(np.array(states))  # FIXME     
        history = self.action_nn.fit(states, targets, epochs=1, verbose=0)
        loss = history.history['loss'][0]

        return loss

    def remember(self, prev_obs, action, reward, obs, done):
        self.memory.append((prev_obs, action, reward, obs, done))

    def up_target_nn(self):
        weights = self.action_nn.get_weights()
        target_weights = self.target_nn.get_weights()
        for i in range(len(target_weights)):
            target_weights[i] = weights[i] * self.tau + target_weights[i] * (1 - self.tau)
        self.target_nn.set_weights(target_weights)

    def save_model(self, name):
        self.action_nn.save('{}/{}_action'.format(CONFIG_PATH, name))
        self.target_nn.save('{}/{}_target'.format(CONFIG_PATH, name))

    def load_model(self, name):
        action_nn = load_model('{}/{}_action'.format(CONFIG_PATH, self.name))
        target_nn = load_model('{}/{}_target'.format(CONFIG_PATH, self.name))
        return action_nn, target_nn


if __name__ == '__main__':
    agent = AgentDQN(input_dim=(3, ), action_dim=3)
    obs = np.zeros((1, 3))
    done = False
    for i in range(10000):
        action = agent.act(obs)
        reward = 1.0
        if i == 9999:
            done = True
        agent.remember(obs, action, reward, obs, done)
    start = time.perf_counter()
    agent.replay()
    print('replay time delta: {}'.format(time.perf_counter() - start))
    agent.up_target_nn()

import numpy as np
import pandas as pd
from collections import deque
import matplotlib.pyplot as plt
import time

from universe.trading_gym.preprocessor import Prepocessor
from universe.trading_gym.trading import TradingEnv
from universe.trading_gym.baseline import TrendFollower
from universe.agent_dqn import AgentDQN
from universe import utils


log = utils.get_logger(logger_name='gym_room.py', fh_log_lv='debug', ch_log_lv='debug')
log_monitor = utils.get_logger(logger_name='behavior.py', fh_log_lv='debug', ch_log_lv='debug')
DATA_PATH = 'D:/dl_data'
# DATA_PATH = '/Users/lokhiufung/Documents/crypto_data/raw'

config = {
        'pdt': 'XBTUSD',
        'exch': 'BMEX',
        'initial_bal': 1.0,
        'max_recent_trade_size': 30,
        'mmd': 0.2,
        'leverage': 10,
        'start_date': '2018-01-01',
        'end_date': '2018-01-03',
        'fee': {'mfee': - 0.025 / 100, 'tfee': 0.075 / 100},
        'filled_dist_params': {'intervals': [(0, 0), (0, 50), (50, 1000), (1000, 5000)], 'weights': [0.1, 0.3, 0.5, 0.1]},
        'data_root_path': DATA_PATH
    }


def random_action_idx(action_space, p=[0.1, 0.8, 0.1]):
    action_idx = np.random.choice(range(len(action_space)), p=p)
    return action_idx


def baseline():
    print('Baseline Start!')
    agent = TrendFollower()
    env = TradingEnv(config)
    cum_reward = []
    done = False
    mid_px = 0.0
    pos_side = 0
    pos_expos = 0.0
    order_qty = 100
    start = time.perf_counter()
    while not done:
        side, order_type, order_qty = agent.act(pos_side, pos_expos, order_qty)
        order = (side, order_type, order_qty, mid_px)
        observations, reward, done, info = env.step(order)
        if observations:
            ts = observations['ts']
            bid, bid_qty, ask, ask_qty = observations['quote']
            pos_side, pos_expos = observations['acct_summary']['pos_side'], observations['acct_summary']['pos_expos']
            mid_px = (bid + ask) / 2
            agent.up_ma(mid_px)
            if order[0] != 0:
                log_monitor.debug('{} - action_side {} order_qty {} pos_side {} pos_expos {} fast_ma {} slow_ma {} long_threshold {} short_threshold {}'.format(ts, order[0], order[2], pos_side, pos_expos, agent.fast_ma, agent.slow_ma, agent.long_threshold, agent.short_threshold))
                log_monitor.debug('{} - acct_summary {}'.format(ts, observations['acct_summary']))
            log.debug('{} - mid_px {} reward {} observations {}'.format(ts, mid_px, reward, observations))
            env.render()
    print('Done! time delta {}'.format(time.perf_counter() - start))
    plt.plot(np.cumsum(cum_reward))
    plt.show()     


def main():
    t_step_size = 10
    num_episodes = 50
    preprocess = Prepocessor(max_lv=1, t_step_size=t_step_size)
    env = TradingEnv(config)
    agent = AgentDQN(input_dim=(preprocess.input_dim, ), action_dim=3)
    action_space = env.action_space
    epsilon = 0.5  #  initial exploration rate
    final_epsilon = 0.01  # final exploration rate
    exploration_steps = 100000  # for decaying exploration
    
    decay = (epsilon - final_epsilon) / exploration_steps  # exploration reduced by this amount
    count = 0
    # for plotting
    q_value_list = []
    reward_list = []
    loss_list = []
    # episode count
    episode_count = 1
    for episode in range(num_episodes):
        
        start = time.perf_counter()  # time needed for 1 episode
    
        done = False  # reset done
       
        env.reset()  # reset acct status to initial values
        state1 = np.zeros((1, preprocess.input_dim)) # FIXME
        while not done:
            
            # take action
            q_values, action_idx = agent.act(state1)
            q_value_list.append(np.amax(q_values))
            # epsilon greedy policy
            if np.random.random() < epsilon:
                action_idx = random_action_idx(action_space)
            if epsilon > final_epsilon:
                # decay
                epsilon -= decay
            action = (action_space[action_idx], 0, 100)

            observations, reward, done, info = env.step(action)
            if observations:
                quote_update = observations['quote']
                ts, bid, bid_qty, ask, ask_qty = quote_update
                if 'trade_update' in observations.keys() and observations['trade']:  # FIXME
                    trade_update = observations['trade']
                    ts_t, t_px, t_side, t_qty = trade_update
                else:
                    ts_t, t_px, t_side, t_qty = 0.0, 0.0, 0, 0.0  # if no trade
                log.debug('{} - observations {} info {}'.format(ts, observations, info))
                # ts = utils.encode_ts(utils.str_to_ts(ts))  # encoded representation of timestamp

                # get data from preprocessor
                state2 = preprocess.get_obs_vec(bid, bid_qty, ask, ask_qty, observations['acct_summary'], t_px, t_side, t_qty)
                # cache memory for exprience replay
                agent.remember(state1, action_idx, reward, state2, done)
                log_monitor.debug('episode {} ts {} q_values {} max_q_value {} action {} state1 {} state2 {}'.format(episode, ts, list(q_values.ravel()), np.amax(q_values), action, list(state1), list(state2)))
                if count > agent.replay_size:
                    if count % 1000 == 0:
                        # experience replay
                        loss = agent.replay()
                        print('Experience replaying !!!')
                        log_monitor.debug('episode {} ts {} loss {}'.format(episode, ts, loss))
                        reward_list.append(reward)
                        q_value_list.append(max(q_values.ravel()))
                        loss_list.append(loss)
                    if count % 4000 == 0:
                        agent.up_target_nn()
                # update the current state
                # print(state2.shape)
                state1 = state2
                count += 1
        log_monitor.debug('episode {} Done! count {} time delta {}'.format(episode, count, time.perf_counter() - start))
        episode_count += 1  # go to next episode
    fig, ax = plt.subplots(2, 1, figsize=(30, 20))
    ax[0].plot(np.cumsum(reward_list), color='blue')
    ax[1].plot(q_value_list, label='Q value', color='red')
    ax[1].plot(pd.Series(q_value_list).rolling(1000).mean(), color='red', alpha=0.4)
    ax[2].plot(loss_list, color='green', label='loss')
    ax[2].plot(pd.Series(loss_list).rolling(1000).mean(), color='green', label='loss')
    ax[0].legend()
    ax[1].legend()
    ax[2].legend()
    plt.show()

def test():
    env = TradingEnv(config)
    action_space = env.action_space
    for i in range(100000):
        order = (action_space[random_action_idx(action_space)], 0, 100, 10000)
        observations, reward, done, info = env.step(order)
        log.debug('observations {}'.format(observations))
        env.render()

if __name__ == '__main__':
    baseline()
    

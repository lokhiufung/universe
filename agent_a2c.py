import tensorflow as tf
from tensorflow.keras import layers


class Model(object):
    """
    actor-critic model, output pi and value
    """
    def __init__(self, input_dim, action_dim):
        self.input_dim, self.action_dim = input_dim, action_dim
        self.actor, self.critic = self.build_model()
    
    def build_model(self):
        input_ = tf.keras.Input(shape=self.input_dim)
        x = layers.Dense(1024, activation='relu')(input_)
        x = layers.Dense(1024, activation='relu')(x)
        value = layers.Dense(1)(x)
        policy = layers.Dense(self.action_dim, activation='sigmod')(x) 

        actor = tf.keras.Model(inputs=self.input_, outputs=policy)
        critic = tf.keras.Model(inputs=self.input_, outputs=value)
        return actor, critic
    
    def train(self):
        pass


class AgentA2C(object):
    def __init__(self, input_dim, action_dim):
        self.model = Model(input_dim, action_dim)

    def act(self, observations):
        action_sample = self.model.actor.predict(observations)
        return action_sample

    def _value_loss(self, returns, value):
        # value loss is typically MSE between value estimates and returns
        return  * tf.keras.losses.mean_squared_error(returns, value)
   
   def train(self, minibatches):
        aggregate_gradient = 0.0
        # expriences received from parallel envs
        for exp in experiences:
            aggregate_gradient += self.step_model.get_gradients(minibatches)
        self.train_model.up_params_by_gradients(average(aggregate_gradient))  # average over the updates


    

        

        